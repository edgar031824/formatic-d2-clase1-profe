function onMouseOver(iobElement) {
	let obSaveBtn = iobElement ? iobElement.currentTarget.previousElementSibling : null,
		arClassList = obSaveBtn ? obSaveBtn.classList : null;

	if (arClassList) {
		obSaveBtn.classList.remove('o-hidden');
		obSaveBtn.classList.add('o-visible');
	}

};

function onMouseOut(iobElement) {
	let obSaveBtn = iobElement ? iobElement.currentTarget.previousElementSibling : null,
		arClassList = obSaveBtn ? obSaveBtn.classList : null;

	if (arClassList) {
		obSaveBtn.classList.remove('o-visible');
		obSaveBtn.classList.add('o-hidden');
	}

}